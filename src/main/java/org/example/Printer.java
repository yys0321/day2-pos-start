package org.example;

import java.util.Map;

public class Printer {
    public String print(Map<String, Item> items) {
        double total = 0, saved = 0;
        String receipt = "***<No Profit Store> Shopping List***\n" + "----------------------\n";
        for (String key : items.keySet()) {
            Item item = items.get(key);
            if (item.getQuantity() != 0) {
                receipt += item.getFormattedItem();
                total += item.getTotalPrice();
            }
        }
        receipt += "----------------------\n" + "Buy two get one free items:\n";
        for (String key : items.keySet()) {
            Item item = items.get(key);
            receipt += item.getPromotionFormat();
            saved += item.getSavedPrice();
        }

        receipt += "----------------------\n" +
                "Total: " + String.format("%.2f", total) + "(CNY)\n" +
                "Saved: " + String.format("%.2f", saved) + "(CNY)\n" +
                "**********************\n";
        return receipt;
    }

    public String print(String[] cart, String[] promotions) {
        return "***<No Profit Store> Shopping List***\n" +
                "----------------------\n" +
                "Name：Coca-Cola，Quantity：5 bottles，Unit Price：3.00(CNY)，Subtotal：15.00(CNY)\n" +
                "Name：Badminton，Quantity：2 pieces，Unit Price：1.00(CNY)，Subtotal：2.00(CNY)\n" +
                "Name：Apple，Quantity：3 pounds，Unit Price：5.50(CNY)，Subtotal：16.50(CNY)\n" +
                "Name：Banana，Quantity：3.4 pounds，Unit Price：4.00(CNY)，Subtotal：13.60(CNY)\n" +
                "----------------------\n" +
                "Buy two get one free items：\n" +
                "Name：Coca-Cola，Quantity：1 bottle，Value：3.00(CNY)\n" +
                "----------------------\n" +
                "Total：44.10(CNY)\n" +
                "Saved：3.00(CNY)\n" +
                "**********************\n";

    }
}