package org.example;

public class Item {

    private String name;
    private String unit;
    private Boolean hasPromotion;
    private double price;
    private double quantity;
    private int freeQuantity;

    private double totalPrice;
    private double savedPrice;

    public Item(String name, double price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.hasPromotion = false;
        this.quantity = 0;
        this.freeQuantity = 0;

        this.totalPrice = 0;
        this.savedPrice = 0;
    }

    public void setPromotion(Boolean hasPromotion) {
        this.hasPromotion = hasPromotion;
    }

    public void addQuantity(double quantity) {
        this.quantity += quantity;
        this.totalPrice += (quantity * price);
        if (this.hasPromotion && this.quantity > 2) {
            this.freeQuantity = (int)this.quantity / 3;
            this.savedPrice = this.freeQuantity * this.price;
        }
    }

    public double getQuantity() {
        return this.quantity;
    }

    public double getTotalPrice() {
        return this.totalPrice;
    }

    public double getSavedPrice() {
        return this.savedPrice;
    }

    public String getFormattedItem() {
        return "Name: " + this.name +
                ", Quantity: " + (this.quantity % 1 == 0 ? String.format("%.0f", this.quantity) : String.format("%.1f", this.quantity)) +
                " " + this.unit +
                (this.quantity > 1 ? "s" : "") +
                ", Unit Price: " + String.format("%.2f", this.price) +
                "(CNY), Subtotal: " + String.format("%.2f", this.totalPrice) + "\n";
    }

    public String getPromotionFormat() {
        if (this.freeQuantity > 0) {
            return "Name: " + this.name +
                    ", Quantity: " + this.freeQuantity +
                    " " + this.unit + (this.freeQuantity > 1 ? "s" : "") +
                    ", Value: " + String.format("%.2f", this.savedPrice) + "(CNY)\n";
        }
        return "";
    }

    public void resetQuantity() {
        quantity = 0.0;
    }

    public void resetPromotion() {
        hasPromotion = false;
    }
}