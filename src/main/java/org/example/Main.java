package org.example;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> allItems = PosDataLoader.loadAllItems();

        PosSystem posSystem = new PosSystem(allItems);

        String[] promotion = PosDataLoader.loadPromotion();
        String[] cart = PosDataLoader.loadCart();

        posSystem.loadPromotion(promotion);
        posSystem.calculateCart(cart);
        posSystem.printReceipt();
    }
}