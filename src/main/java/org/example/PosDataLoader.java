package org.example;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class PosDataLoader {

    public static Map<String, String> loadAllItems() {
        Map<String, String> items = new LinkedHashMap<String, String>() {
        };
        //barcode, name, unitPrice, unit
        items.put("ITEM000001", "Coca-Cola,3,bottle");
        items.put("ITEM000002", "Sprite,3,bottle");
        items.put("ITEM000003", "Badminton,1,piece");
        items.put("ITEM000004", "Instant noodles,4.5,pack");
        items.put("ITEM000005", "Apple,5.5,pound");
        items.put("ITEM000006", "Banana,4,pound");

        return items;
    }

    public static Map<String, Item> formatItems(Map<String, String> items) {
        Map<String, Item> result = new HashMap<>();
        // items are loaded dynamically into a HashMap of Item class
        for(String key : items.keySet()) {
            String[] details = items.get(key).split(",");
            result.put(key, new Item(details[0], Double.parseDouble(details[1]), details[2]));
        }
        return result;
    }

    public static String[] loadCart() {
        return new String[]{"ITEM000001", "ITEM000001", "ITEM000001", "ITEM000001",
                "ITEM000001", "ITEM000003", "ITEM000003", "ITEM000005-3", "ITEM000006-3.4"};
    }

    public static String[] loadPromotion() {
        return new String[]{"ITEM000001", "ITEM000003"};
    }
}