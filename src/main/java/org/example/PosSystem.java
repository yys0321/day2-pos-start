package org.example;

import java.util.Map;

public class PosSystem {

    Map<String, Item> items;
    String[] cart;

    Printer printer;

    public PosSystem(Map<String, String> allItems) {
        items = PosDataLoader.formatItems(allItems);
        printer = new Printer();
    }

    public void loadPromotion(String[] promotionItems) {
        for (String item : promotionItems) {
            items.get(item).setPromotion(true);
        }
    }

    public void calculateCart(String[] cart) {
        for (String item : cart) {
            double quantity = 1;
            if (item.contains("-")) {
                String[] itemWithQuantity = item.split("-");
                item = itemWithQuantity[0];
                quantity = Double.parseDouble(itemWithQuantity[1]);
            }
            items.get(item).addQuantity(quantity);
        }
    }

    public void printReceipt() {
        System.out.println(printer.print(items));
    }

    public void resetItemQuantity() {
        for (String key : items.keySet()) {
            items.get(key).resetQuantity();
        }
    }

    public void resetPromotion() {
        for (String key : items.keySet()) {
            items.get(key).resetPromotion();
        }
    }
}